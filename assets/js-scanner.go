package assets

import (
	"log"
	"path"
	"regexp"
	"bitbucket.org/gotraining/snowball/utils"
)

// JsScanner Returns asset list gathered from input
type JsScanner struct{}

// ScanSource Returns array of strings representing modules
func (scanner JsScanner) ScanSource(assetPath string) []ImportStatement {
	source := utils.OpenFile(assetPath)
	assetDir := path.Dir(assetPath)

	moduleExtension := "js"
	importSignature := regexp.MustCompile("(require\\(')(.*)('\\))")
	importMatches := importSignature.FindAllStringSubmatch(source, -1)

	var imports []ImportStatement

	for _, match := range importMatches {
		rawModuleName := match[2]
		relativeImportPath := path.Join(assetDir, rawModuleName)

		if hasCorrectExtension, extensionCheckError := regexp.MatchString("\\."+moduleExtension+"$", rawModuleName); hasCorrectExtension == false {
			relativeImportPath += "." + moduleExtension

			if extensionCheckError != nil {
				log.Fatal(extensionCheckError)
			}
		}

		imports = append(imports, ImportStatement{relativeImportPath, rawModuleName})
	}

	return imports
}


