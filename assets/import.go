package assets

type ImportStatement struct {
	path string
	name string
}
