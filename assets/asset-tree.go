package assets

func BuildAssetTree(entryPath string) Asset {
	rootAsset := NewAsset("root", entryPath)
	return *traverseChildren(&rootAsset)
}

func BuildAssetListFromTree(treeRoot Asset) []Asset {
	flattenedTree := append([]Asset{treeRoot}, flattenBranch(treeRoot)...)

	for i := range flattenedTree {
		for j := range flattenedTree[i].ChildAssets {
			flattenedTree[i].ChildAssets[j].ChildAssets = nil
		}
	}

	return flattenedTree
}

func flattenBranch(asset Asset) []Asset {
	var result []Asset

	result = append(result, asset.ChildAssets...)
	for _, childLeaf := range asset.ChildAssets {
		result = append(result, flattenBranch(childLeaf)...)
	}

	return result
}

func traverseChildren(asset *Asset) *Asset {
	// TODO pick scanner based on module extension
	scanner := JsScanner{}
	importStatements := scanner.ScanSource(asset.Path)

	var childAssets []Asset

	for _, importStatement := range importStatements {
		childAsset := NewAsset(importStatement.name, importStatement.path)
		childAssets = append(childAssets, *traverseChildren(&childAsset))
	}
	asset.ChildAssets = childAssets

	return asset
}
