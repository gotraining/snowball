package assets

type Asset struct {
	Name        string
	Path        string
	ChildAssets []Asset
}

func NewAsset(name string, path string) Asset {
	return Asset{name, path, nil}
}
