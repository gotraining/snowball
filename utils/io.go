package utils

import (
	"io/ioutil"
	"log"
)

func OpenFile(path string) string {
	file, fileError := ioutil.ReadFile(path)

	if fileError != nil {
		log.Fatal(fileError)
	}

	return string(file)
}
