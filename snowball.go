package main

import (
	"bitbucket.org/gotraining/snowball/assets"
	"fmt"
	"os"
)

func main() {
	args := os.Args[1:]
	if len(args) == 0 {
		fmt.Println("entry path missing")
		return
	}

	entryPath := args[0]

	fmt.Printf("running for entry path '%s'...\n", entryPath)

	tree := assets.BuildAssetTree(entryPath)

	assetsList := assets.BuildAssetListFromTree(tree)

	for _, assetEntry := range assetsList {
		fmt.Printf("Asset name:\t%s\tDependencies:\t%+v\n", assetEntry.Name, assetEntry.ChildAssets)
	}
}
